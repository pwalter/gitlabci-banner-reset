// ==UserScript==
// @name         gitlabci-banner-reset
// @namespace    https://www.ebi.ac.uk
// @version      0.3
// @description  Remove annoying orange banner from gitlabci
// @author       Peter Walter (pwalter@ebi.ac.uk)
// @match        https://gitlabci.ebi.ac.uk/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    if (document.querySelector('.header-message').innerText === 'EBI Gitlab EE , Live Instance') {
        document.querySelector('.header-message').remove();
        document.querySelector('.navbar').style.top=0;
        document.querySelector('.content-wrapper').style.marginTop='40px';
        document.querySelector('.nav-sidebar').style.top='40px';
    }
})();