# gitlabci-banner-reset

Remove annoying orange banner from gitlabci


Requires [TamperMonkey](https://tampermonkey.net/)

Install https://gitlabci.ebi.ac.uk/pwalter/gitlabci-banner-reset/raw/master/gitlabci-banner-reset.user.js